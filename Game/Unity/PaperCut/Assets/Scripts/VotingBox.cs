﻿using UnityEngine;
using System.Collections;

public class VotingBox : MonoBehaviour {
	public bool isA;
	public bool isInsideVotingBox;

	void OnTriggerEnter2D(Collider2D coll) {
		//Debug.Log ("hi");
		if (coll.gameObject.tag == "Player1"||coll.gameObject.tag == "Player2"||coll.gameObject.tag == "Player3"||coll.gameObject.tag == "Player4") {
			if(isA){
				Handler.voteA ++;
				Debug.Log ("A: "+Handler.voteA);
			}
			else if(isA == false){
				Handler.voteB ++;
				Debug.Log ("B: "+Handler.voteB);
			}
		}

		
	}
	void OnTriggerExit2D(Collider2D coll) {
		if (coll.gameObject.tag == "Player1"||coll.gameObject.tag == "Player2"||coll.gameObject.tag == "Player3"||coll.gameObject.tag == "Player4") {
			if (isA) {
				Handler.voteA --;
				Debug.Log ("A: " + Handler.voteA);
			}
			if (isA == false) {
				Handler.voteB --;
				Debug.Log ("B: " + Handler.voteB);
			}
		}
	}
}
