﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class storyboard : MonoBehaviour {
	public Font typewriter;
    public  Renderer rend;
    public 	Texture2D currentText;
    public Texture2D[] storyText;
    public  int currentStory = 0;
	public  int story;
    private Text dialog;
	private Text optionA;
	private Text optionB;
    public GameObject objDialog;
	public GameObject objOptionA;
	public GameObject objOptionB;
	public static storyboard instance;
	public GameObject restart;

	//Objects
	private bool necklace = false;
	private bool gnome = false;
	private bool apple = false;

	public GameObject DebuggerObject;

    public void nextStory() {  
		if (currentStory == -1) {
			dialog.text = "Once upon a time, in a kingdom far far away. \n There lived this poor fella, who had been cursed with inability to make his own decisions. But there were these voices in his head, telling him what to do. Usually they were arguring... and some times they were cheering about beer. But ONE day day they brought him out of his house onto the winding road, and he found himself standing before a crossroad. \n<b>A.</b> He looks to the right and sees clouds of <b>doom</b>, \n<b>B.</b> looking left he sees flowers in <i>bloom</i>... Which way to go?";
			optionA.text = "<b>A.</b> He looks to the right and sees clouds of <b>doom</b>";
			optionB.text = "<b>B.</b> looking left he sees flowers in <i>bloom</i>... ";
			currentStory = 0;

		} else if (currentStory == 0) {
			if (Debugger.Awon == true) {
				dialog.text = "He walks down the path for many hours alone, when suddenly he is met by a handsome man astride a mighty steed. \n'Ho traveler! Have you by any chance seen a princess pass by?' The poor fell wonder what to answer, \nto speak the truth or to lie?";
				optionA.text = "A. Speak truth";
				optionB.text = "B. Lie";
				currentStory = 1;
			} else if (Debugger.Awon == false) {
				dialog.text = "He walks down the path for many hours alone, when suddenly a <i>mysterious</i> Garden Gnome blocks his path.\n \n It stares at him with its painted beedy eyes, but is standing quite still, as sculpture without life...";
				optionA.text = "A. Pick up gnome";
				optionB.text = "B. kick gnome";
				currentStory = 2;
			}

		} else if (currentStory == 1) {		
			if (Debugger.Awon == true) {				
				dialog.text = "'I have not seen your princess I am sorry to say.' The poor fellow speaks the truth and the knight frowns in distain. \n'Well you are a useless sod aren't you?' he sneers and kicks him aside. \n'Move away then foul peasant, I have orders I must abide!' \nSo the poor fellow watch the knight disappear down the winding road, then pulls himself up with nothing to his name, but the mud stuck in his coat.\n\n The poor fellow reaches a small village set alight with doom and flames. And he approach a weeping witch clutching in her arms a golden chain.";
				optionA.text = "A. Ask what happened";
				optionB.text = "B. Steal her necklace";
				currentStory = 4;
			} else if (Debugger.Awon == false) {				
				dialog.text = "'I think I saw her back when I last took a turn, follow the road and go left and you should be right on her trail.' the poor fellow lied smoothly and the knight smiled bright. \n'Thank you good sir, you have done your king and country a great service. Take this for our troubles and good speed ahead.' The knight throws him an apple and rides on ahead. The fellow stare at the fruit and wonders what to do with it.";
				optionA.text = "A. Eat the apple";
				optionB.text = "B. Save the apple for later";
				currentStory = 3;
				apple = true;
			}
		} else if (currentStory == 2) {
			if (Debugger.Awon == true) {
				dialog.text = "The poor fellow pick up the gnome which utters no word of protest, and he ventures forth on his journey, having passed his first test.\n\n The poor fellow reaches a small village set alight with doom and flames. And he approach a weeping witch clutching in her arms a golden chain.";
				optionA.text = "A. Ask what happened";
				optionB.text = "B. Steal her necklace";
				currentStory = 5;
				gnome = true;
			} else if (Debugger.Awon == false) {
				dialog.text = "'The fuck are you doing?!' The Gnome cried in dispair. \n'You broke my hat you fool! I'll make you pay for that!' The gnome then pulled a rifle from its back, and shot a hole through our hero ending his story just like that... [The End]";
				optionA.text = "";
				optionB.text = "";
				currentStory = 6;
				DebuggerObject.SendMessage ("hideTimer");

			}
		} else if (currentStory == 3) {
			if (Debugger.Awon == true) {
				dialog.text = "The poor fellow eats the apple, then stiffens at the taste. He is suddenly very sleepy, and he falls over on the road. Which for the voices in his head probably would have known would happen, had they ever read a proper fairy tale before! [The End]";
				optionA.text = "";
				optionB.text = "";
				currentStory = 7;
				DebuggerObject.SendMessage ("hideTimer");
			} else if (Debugger.Awon == false) {
				dialog.text = "The poor fellow pockets the apple with a smile on his face, and continues on his journey an apple richer, or heavier depending on how you see it. \n\nThe poor fellow reaches a small village set alight with doom and flames. And he approach a weeping witch clutching in her arms a golden chain.";
				optionA.text = "A. Ask what happened";
				optionB.text = "B. Steal her necklace";
				currentStory = 8;
			}
		} else if (currentStory == 4) {
			if (Debugger.Awon == true) {
				dialog.text = "'What is this dear witch? Why are you weeping so?' \n'The feline terror has destroyed my village and brought doom to us all.' The poor fellow leaves the witch, to mourn her loss in peace. \n\nOutside the village he finds a trail of flame and fire, and he follows it without thinking, for that is what the narrator desires. No dragon awaits him however, and he comes to a hold, staring oddly at a kitten, sleeping sound on a stone.";
				optionA.text = "A. Poke the Kitten";
				optionB.text = "B. Sneak around the kitten";
				currentStory = 9;
			} else if (Debugger.Awon == false) {
				dialog.text = "'What in the world are you doing?!' cries the witch in dispair, but the poor fellow is already running with a new necklace to wear. \n\nOutside the village he finds a trail of flame and fire, and he follows it without thinking for that is what the narrator desires. No dragon awaits him however, and he comes to a hold, staring oddly at a kitten, sleeping sound on a stone.";
				optionA.text = "A. Poke the Kitten";
				optionB.text = "B. Sneak around the kitten";
				currentStory = 10;
			}
		} else if (currentStory == 5) {
			if (Debugger.Awon == true) {
				dialog.text = "'What is this dear witch? Why are you weeping so?' 'The feline terror has destroyed my village and brought doom to us all.' The poor fellow leaves the witch, to mourn her loss in peace. \n\nOutside the village he finds a trail of flame and fire, and he follows it without thinking, for that is what the narrator desires. No dragon awaits him however, and he comes to a hold, staring oddly at a kitten, sleeping sound on a stone.";
				optionA.text = "A. Poke the Kitten";
				optionB.text = "B. Sneak around the kitten";
				currentStory = 9;
			} else if (Debugger.Awon == false) {
				dialog.text = "'What in the world are you doing?!' cries the witch in dispair, but the poor fellow is already running with a new necklace to wear. \n\nOutside the village he finds a trail of flame and fire, and he follows it without thinking for that is what the narrator desires. No dragon awaits him however, and he comes to a hold, staring oddly at a kitten, sleeping sound on a stone.";
				optionA.text = "A. Poke the Kitten";
				optionB.text = "B. Sneak around the kitten";
				currentStory = 10;
			}
		} else if (currentStory == 8) {
			if (Debugger.Awon == true) {
				dialog.text = "'What is this dear witch? Why are you weeping so?' 'The feline terror has destroyed my village and brought doom to us all.' The poor fellow leaves the witch, to mourn her loss in peace. \n\nOutside the village he finds a trail of flame and fire, and he follows it without thinking, for that is what the narrator desires. No dragon awaits him however, and he comes to a hold, staring oddly at a kitten, sleeping sound on a stone. A. Poke the kitten B. Sneak around the kitten";
				optionA.text = "A. Poke the Kitten";
				optionB.text = "B. Sneak around the kitten";
				currentStory = 9;
			} else if (Debugger.Awon == false) {
				dialog.text = "'What in the world are you doing?!' cries the witch in dispair, but the poor fellow is already running with a new necklace to wear. \n\nOutside the village he finds a trail of flame and fire, and he follows it without thinking for that is what the narrator desires. No dragon awaits him however, and he comes to a hold, staring oddly at a kitten, sleeping sound on a stone. A. Poke the kitten B. Sneak around the kitten";
				optionA.text = "A. Poke the Kitten";
				optionB.text = "B. Sneak around the kitten";
				currentStory = 10;
			}
		} else if (currentStory == 9) {
			if (Debugger.Awon == true) {
				dialog.text = "The poor fellow pokes the cat gently, smiles widely at the feel of soft fur, but then there's a thunderous rumble and the kittens begins to blur. \n\nThe small creature grows, right before his eyes, into a great furry monster that breathes fire... oh my.";
				//Option A
				if (necklace == true) {
					optionA.text = "A. Put the necklace on the kitten";
				} else {
					optionA.text = "A. Plead Mercy";
				}
				//Option B
				if (gnome == true) {
					optionB.text = "Throw the gnome at it!";
				} else if (apple == true) {
					optionB.text = "Throw the apple at it!";
				} else {
					optionB.text = "Fight it!";
				}
				currentStory = 11;


			} else if (Debugger.Awon == false) {
				dialog.text = "The poor fellow sneaks around the kitten and find to his great surprise, five bottles of vodka, empty and of great size...";
				//Option A
				if (necklace == true)
					optionA.text = "A. Put the necklace on the kitten";
				else
					optionA.text = "A. Kill the kitten!";
				//Option B
				if (gnome == true)
					optionB.text = "Place the gnome in front of it";
				else if (apple == true)
					optionB.text = "Throw the apple at it!";
				else
					optionB.text = "Leave it";
				currentStory = 12;
			}
		} else if (currentStory == 10) {
			necklace = true;
			if (Debugger.Awon == true) {
				dialog.text = "The poor fellow pokes the cat gently, smiles widely at the feel of soft fur, but then there's a thunderous rumble and the kittens begins to blur. \n\nThe small creature grows, right before his eyes, into a great furry monster that breathes fire... oh my.";
				//Option A
				if (necklace == true)
					optionA.text = "A. Put the necklace on the kitten";
				else
					optionA.text = "A. Plead Mercy";
				//Option B
				if (gnome == true)
					optionB.text = "B. Throw the gnome at it!";
				else if (apple == true)
					optionB.text = "B. Throw the apple at it!";
				else
					optionB.text = "B. Fight it!";
				
				currentStory = 11;
			} else if (Debugger.Awon == false) {
				dialog.text = "The poor fellow sneaks around the kitten and find to his great surprise, five bottles of vodka, empty and of great size...";
				//Option A
				if (necklace == true)
					optionA.text = "A. Put the necklace on the kitten";
				else
					optionA.text = "A. Kill the kitten!";
				//Option B
				if (gnome == true)
					optionB.text = "B. Place the gnome in front of it";
				else if (apple == true)
					optionB.text = "B. Throw the apple at it!";
				else
					optionB.text = "B. Leave it";
				currentStory = 12;
			}
		} else if (currentStory == 11) {
			optionA.text = "";
			optionB.text = "";
			if (Debugger.Awon == true) {
				if (necklace == true) {
					dialog.text = "Should probably had done that while its sleeping. Now its impossible to reach. \n\nThe poor fellow and cries and pleads for his life, but is turned into a crips, what an end for the poor guy... [The End]";
					currentStory = 15;
					DebuggerObject.SendMessage ("hideTimer");
				} else {
					dialog.text = "The poor fellow and cries and pleads for his life, but is turned into a crips, what an end for the poor guy... [The End]";
					currentStory = 13;
					DebuggerObject.SendMessage ("hideTimer");
				}
			}
			//Option B
			else if (Debugger.Awon == false) {
				if (gnome == true) {
					dialog.text = "The poor fellow throws the gnome at the kitten with a cry, and to his great shock the statue moves and attacks with a battle cry. The gnome kicks and punch beating the monster to a pulp, and after it wanders off leaving the poor fellow to silently gulp. \n\nThe poor fellow returns to his city, quite shocked by this design, and he is greeted as a hero, well done team you made it, this time... [The End]";
					currentStory = 17;
					DebuggerObject.SendMessage ("hideTimer");
				} else if (apple == true) {
					dialog.text = "The poor fella throw the apple he got from the knight at the kittens face, and it swallows it whole. The monster starts to shake and falls over, very much dead... \n\nThe poor fellow returns to his city, quite shocked by this design, and he is greeted as a hero, well done team you made it, this time... [The End]";
					currentStory = 16;
					DebuggerObject.SendMessage ("hideTimer");
				} else {
					dialog.text = "The poor fellow brings up his fists and fights for his life, but is turned into a crips, what an end for the poor guy... [The End]";
					currentStory = 14;
					DebuggerObject.SendMessage ("hideTimer");
				}
				DebuggerObject.SendMessage ("hideTimer");
			}
		
		} 
		else if (currentStory == 12) {
			optionA.text = "";
			optionB.text = "";
			if (Debugger.Awon == true) {
				if (necklace == true) {
					dialog.text = "The poor fellow puts the necklace careful around the kittens neck, and when the thing locks its sudden a collar what the heck? The kitten purs and stretches and the poor fellow picks it up with a smile, bringing it home with him is the first decision he has made in a while. \n\nThe poor fellow returns to his city, quite shocked by this design, and he is greeted as a hero, well done team you made it, this time... [The End]";
					currentStory = 20;
					DebuggerObject.SendMessage ("hideTimer");
				} else {
					dialog.text = "The poor fellow wring the neck of the cat while it is asleep, and walks home along the road feeling quite off of what he did... \n\nThe poor fellow returns to his city, quite shocked by this design, and he is greeted as a hero, well done team you made it, this time... [The End]";
					currentStory = 18;
					DebuggerObject.SendMessage ("hideTimer");
				}
			}
			//Option B
			if (Debugger.Awon == false) {
				if (gnome == true) {
					dialog.text = "The poor fellow place the gnome in front of the kitten and waits a while, the cat open its eyes and purs in delight at this new sight. The kitten and the gnome ends up the worlds best friends, and just like that, the threat of the monster cat ends. \n\nThe poor fellow returns to his city, quite shocked by this design, and he is greeted as a hero, well done team you made it, this time... [The End]";
					currentStory = 22;
					DebuggerObject.SendMessage ("hideTimer");
				} else if (apple == true) {
					dialog.text = "The poor fellow throws the apple at the kitten, watching it bounche off its fur, but then there's a thunderous rumble and the kittens begins to blur. The small creature grows, right before his eyes, into a great furry monster that breathes fire... oh my. The poor fellow and cries and pleads for his life, but is turned into a crips, what an end for the poor guy... [The End]!";
					currentStory = 21;
					DebuggerObject.SendMessage ("hideTimer");
				} else {
					dialog.text = "The poor fellow leaves the kitten and returns home to his village, only to be burned to death the next day when the cat monster returns for another pillage. [The End]";
					currentStory = 19;						
					DebuggerObject.SendMessage ("hideTimer");
				}
			}
			DebuggerObject.SendMessage("hideTimer");
		}

			

		//Rend the story scene
		Debug.Log (currentStory);
		currentText = storyText[currentStory];
		rend.material.mainTexture = currentText;
		Handler.clearVoting ();
    }

	public void restartGame(){
		currentStory = -1;
		nextStory ();
	}

	void Awake(){
		if (instance == null) {
			instance=this;
		}
	}
	void Start () {
        rend = GetComponent<Renderer>();
        rend.material.mainTexture = storyText[0];
        dialog = objDialog.GetComponent<Text>();
		optionA = objOptionA.GetComponent<Text>();
		optionB = objOptionB.GetComponent<Text>();
		dialog.font = typewriter;
		optionA.font = typewriter;
		optionB.font = typewriter;
    }
	

}
