﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeReading : MonoBehaviour {
	public GameObject votingScene;
	public Text counterReadingText;
	public Text counterText;
	float timeLeft = 10.0f;
	

	// Use this for initialization
	void Start () {
		//Debug.Log("Hello");
	}
	
	// Update is called once per frame
	void Update () {
		timeLeft -= Time.deltaTime;
		counterReadingText.text = timeLeft.ToString("00");
		
		if (timeLeft < 0){
			Debug.Log("Time gone");
			votingScene.SetActive(true);
			counterReadingText.enabled =false;
			counterText.enabled =true;
		}
	}
}
