﻿using UnityEngine;
using XInputDotNetPure;

public class PlayerMovement : MonoBehaviour {
	
	public float speed = 0f;
    Vector2 movementInput = new Vector2 (0.0f, 0.0f);

    int playerNr;

    PlayerIndex playerIndex;
    GamePadState state;
    GamePadState prevState;

	void Start(){
		speed = 9f;
	}

	void giveNumber (int number){
		playerNr = number;
	}

    void Update(){
        if (!prevState.IsConnected)//in case of a disconnect
        {
            PlayerIndex testPlayerIndex = (PlayerIndex)playerNr;
            GamePadState testState = GamePad.GetState(testPlayerIndex);
            if (testState.IsConnected)
            {
                Debug.Log(string.Format("GamePad found {0}", testPlayerIndex));
                playerIndex = testPlayerIndex;
            }
        }

        prevState = state;
        state = GamePad.GetState(playerIndex);


        if (prevState.Buttons.A == ButtonState.Released && state.Buttons.A == ButtonState.Pressed)
            Debug.Log(string.Format("Player {0} pressed A!", playerNr + 1));

        if (prevState.Buttons.B == ButtonState.Released && state.Buttons.B == ButtonState.Pressed)
            Debug.Log(string.Format("Player {0} pressed B!", playerNr + 1));

        if (prevState.Buttons.X == ButtonState.Pressed && state.Buttons.X == ButtonState.Pressed)
            Debug.Log(string.Format("Player {0} pressed X!", playerNr + 1));

        if (prevState.Buttons.Y == ButtonState.Released && state.Buttons.Y == ButtonState.Pressed)
            Debug.Log(string.Format("Player {0} pressed Y!", playerNr + 1));

        if (prevState.Buttons.LeftShoulder == ButtonState.Released && state.Buttons.LeftShoulder == ButtonState.Pressed)
            Debug.Log(string.Format("Player {0} pressed LB!", playerNr + 1));

        if (prevState.Buttons.RightShoulder == ButtonState.Released && state.Buttons.RightShoulder == ButtonState.Pressed)
            Debug.Log(string.Format("Player {0} pressed RB!", playerNr + 1));

        if (prevState.Buttons.Back == ButtonState.Released && state.Buttons.Back == ButtonState.Pressed)
            Debug.Log(string.Format("Player {0} pressed Back!", playerNr + 1));

        movementInput = new Vector2(state.ThumbSticks.Left.X, state.ThumbSticks.Left.Y) * speed;
    }

    void FixedUpdate () {
		GetComponent<Rigidbody2D>().velocity = movementInput;
	}
}