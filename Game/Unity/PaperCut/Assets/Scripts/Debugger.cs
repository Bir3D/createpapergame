﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Debugger : MonoBehaviour {
	public GameObject imageA;
	public GameObject imageB;
	public bool isVoting;
	public static bool Awon;
	public GameObject votingScene;
    public Text counterText;
    float timeLeft = 30.0f;
	public GameObject restart;


	public void resetVotingTime(){
		timeLeft = 30.0f;
	}
    // Use this for initialization
    void Start () {
		imageA.SetActive(false);
		imageB.SetActive(false);
        //Debug.Log("Hello");
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp (KeyCode.Space))
			hideTimer ();
		else if (Input.GetKeyUp (KeyCode.Tab))
			showTimer ();
		
		if(counterText.enabled)
			timeLeft -= Time.deltaTime;

			counterText.text = timeLeft.ToString("00");
			
		
            if (timeLeft < 0 && isVoting == true) {
			isVoting = false;
			Debug.Log ("Time gone");
			votingScene.SetActive(false);		
			imageA.SetActive(false);
			imageB.SetActive(false);

			if (Handler.voteA < Handler.voteB) {
				Awon = false;
				storyboard.instance.nextStory ();
				resetVotingTime ();
				Debug.Log ("B won");
			} else if (Handler.voteA >= Handler.voteB) {
				Awon = true;
				storyboard.instance.nextStory ();
				resetVotingTime ();
				Debug.Log ("A won");
			}
		} 
		else if (timeLeft < 0 && isVoting == false) {
			votingScene.SetActive(true);
			imageA.SetActive(true);
			imageB.SetActive(true);
			resetVotingTime ();
			isVoting = true;
		}
    }



    public void hideTimer()
    {
        counterText.enabled = false;
		restart.SetActive (true);	
    }
    public void showTimer()
    {
        counterText.enabled = true;
		timeLeft = 0;
    }
}
