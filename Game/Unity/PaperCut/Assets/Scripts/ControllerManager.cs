﻿using System.Linq;
using UnityEngine;
using XInputDotNetPure;

public class ControllerManager : MonoBehaviour {

	GameObject[] Players = new GameObject[4]; 
    
	//xInput variables
	GamePadState[] state = new GamePadState[4];
	GamePadState[] prevState = new GamePadState[4];

	// Use this for initialization
	void Start () {
		for (int i = 0; i < 4; ++i)
		{
			PlayerIndex testPlayerIndex = (PlayerIndex)i;
			GamePadState testState = GamePad.GetState(testPlayerIndex);
			if (testState.IsConnected)
			{
				Debug.Log(string.Format("Found Gamepad {0}", testPlayerIndex));
                SpawnPlayer(i);
			}
		}
        Debug.Log("Active players: " + GetActivePlayers());
	}
	
	void Update (){
        for (int i = 0; i < 4; i++)
        {
            prevState[i] = state[i];
            state[i] = GamePad.GetState((PlayerIndex)i);

            if (state[i].Buttons.Start == ButtonState.Pressed && prevState[i].Buttons.Start != ButtonState.Pressed)
            {
                if (IsPlayerActive(i)){DeSpawnPlayer(i);}
                else{                  SpawnPlayer(i);}
            }
        }
    }


    public void PauseGameState(){
		//Pause the game - not implemented
	}

	public void RestartGame(){
        Application.LoadLevel(Application.loadedLevelName);//Restart the game
	}

	//Returns the number of players playing the game
	public int GetActivePlayers(){
		return Players.Count(c => c != null);
	}

	//Tells if a specific player is currently active
	public bool IsPlayerActive(int playerNr){
		return Players[playerNr] != null; 
	}

	public void SpawnPlayer(int playerNr){
		Players[playerNr] = (GameObject)Instantiate(Resources.Load("Player" + (playerNr + 1)));
		Players[playerNr].transform.parent = this.gameObject.transform;
		Players[playerNr].transform.position = new Vector3(2 * playerNr, 0, 0);//Spawnpoint
        Players[playerNr].name = "Player" + (playerNr + 1);
        Players[playerNr].tag = "Player" + (playerNr + 1);
		Players[playerNr].SendMessage ("giveNumber", playerNr);
    }

	public void DeSpawnPlayer(int playerNr){
        Destroy(Players[playerNr]);
    }

	public Vector3 PlayerPos(int playerNr){
		return Players[playerNr].transform.position;
	}
}
