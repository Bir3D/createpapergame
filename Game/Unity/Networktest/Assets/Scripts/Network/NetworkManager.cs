﻿using UnityEngine;
using System.Collections;

public class NetworkManager : MonoBehaviour {
	const string VERSION = "v0.0.1";
	public string roomName = "ourRoom";
	public string playerPrefabVote = "Vote";
	public Transform spawnPoint;

	void Start () {
		//GameObject instance = Instantiate(Resources.Load("Vote", typeof(GameObject))) as GameObject;
		PhotonNetwork.ConnectUsingSettings(VERSION);
	}

	void OnJoinedLobby(){
		RoomOptions roomOptions = new RoomOptions() { isVisible = false, maxPlayers = 4 };
		PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default);
	}

	void OnJoinedRoom() {
		//This is for every player.
		PhotonNetwork.Instantiate ("Vote", spawnPoint.position, spawnPoint.rotation, 0);
	}

}
